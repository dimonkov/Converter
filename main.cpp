#include <iostream>
#include <stdint.h>
#include <stack>
#include <stdexcept>
//Mask for 4 upper bits
const uint64_t fractionMask = (1ULL << 60) - 1ULL;

using namespace std;

//Helper function, determines if a value is in range
bool isInRange(uint32_t lowerB,uint32_t higherB, uint32_t num)
{
    return (lowerB <= num) && (num <= higherB);
}
//Convert numbers to chars for printing
char toHumanHex(uint8_t x)
{
    if(isInRange(0,9, x))
        return (x + '0');
    if(isInRange(10,15, x))
        return x + '7'; // 'A' is 10 symbols away from 7 in ASCII table
    throw invalid_argument("toHumanHex: The value passed cannot be converted to a letter");
}
uint16_t toNumber(char x)
{
    if(isInRange('0','9',x))
        return x - '0';
    if(isInRange('a','f',x))
        return x - 'W'; // 'a' is 10 symbols away from 'W' in ASCII table
    if(isInRange('A','F',x))
        return x - '7'; // 'A' is 10 symbols away from '7' in ASCII table
    throw invalid_argument("toNumber: The value passed cannot be converted to a number");
}

//Fixed point
//at 60 bit from right
struct Decimal
{
    uint64_t UpperPart;
    uint64_t LowerPart;
    Decimal(): UpperPart(0), LowerPart(0)
    {}
};
//Outputs struct in selected base
void printInBase(Decimal dec, ostream& out, uint16_t base, uint16_t decLimit)
{
    if(!isInRange(2,16,base))
        throw invalid_argument("printInBase: Base for conversion must be in [2,16] range");
    //Accumulate letters
    stack<char> chars;
    do
    {
        chars.push(toHumanHex(dec.UpperPart % base));
        dec.UpperPart /= base;
    }
    while(dec.UpperPart != 0ULL);
    //Print them out
    while(!chars.empty())
    {
        out << chars.top();
        chars.pop();
    }

    //If no decimal part, return
    if(dec.LowerPart == 0ULL)
        return;
    out << ',';
    uint64_t num;
    //Print out decimal part, upper 4 bits are reserved for multiplication
    while(dec.LowerPart != 0ULL && decLimit > 0)
    {
        dec.LowerPart *= base;
        num = (dec.LowerPart >> 60); //Take first 4 bits as a separate number
        out << toHumanHex(num); //Convert to char
        dec.LowerPart &= fractionMask;
        decLimit--;
    }
}

struct User_Input
{
    uint16_t numSysFrom;
    uint16_t numSysTo;
    string input;
};

Decimal parseInputString(string& str,uint16_t base)
{
    if(str.length()<= 0)
        throw invalid_argument("parseInputString: String contains no characters");
    Decimal dec;
    char t;
    uint32_t g = 0, temp = 0;
    for(;g < str.length();g++)
    {
        t = str[g];
        if(t == ',' || t == '.')
            break; //Once found separator, stop with the integer part
        temp = toNumber(t);
        if(!isInRange(0,base-1,temp))
            throw invalid_argument("parseInputString: numbers cannot be higher than / equal to number system base!");
        dec.UpperPart *= base; //Trivial conversion
        dec.UpperPart += temp;
    }
    for(uint32_t i = str.length()-1;i > g;i--)
    {
        t = str[i];
        temp = toNumber(t);
        if(!isInRange(0,base-1,temp))
            throw invalid_argument("parseInputString: numbers cannot be higher than / equal to number system base!");
        dec.LowerPart += (((uint64_t)(temp)) << 60); //Push temp into first 4 bits
        dec.LowerPart /= base;
    }
    return dec;
}

User_Input getInput()
{
    User_Input inp;
    cout << "If you want to exit, press CTRL + C any time." << endl << endl;
    while(true)
    {
        cout << "Please specify your input number system: ";
        cin >> inp.numSysFrom;
        if(isInRange(2,16,inp.numSysFrom))
            break;
        else
            cout << "Error!\nOnly number systems in range [2,16] are supported" << endl;
    }

    cout << "Please specify your number: ";
    cin >> inp.input;

    while(true)
    {
        cout << "Please specify number system, that you wish to convert to: ";
        cin >> inp.numSysTo;
        if(isInRange(2,16,inp.numSysTo))
            break;
        else
            cout << "Error!\nOnly number systems in range [2,16] are supported" << endl;
    }
    return inp;
}

int main()
{
    User_Input inp = getInput();
    Decimal dec = parseInputString(inp.input,inp.numSysFrom);

    cout << "Your number is: ";
    printInBase(dec,cout,inp.numSysTo,60);
    cout << endl;

    return 0;
}
